import pandas as pd
import time
import os

from threading import Thread
from datetime import datetime

from flask import render_template

from src.models import History, Data
from src.general import app, db, DELTA, FILES_DIR


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/history')
def history():
    return render_template(
        'history.html',
        data=History.query.all()
    )


@app.route("/file_info/<id>", methods=["GET"])
def file_info(id):
    return render_template(
        "file_info.html",
        data=History.query.filter_by(id=id).first().datas
    )


def look_for_files(time_delta, dir):
    while True:
        files = os.listdir(dir)
        for file in files:
            done = False
            filepath = os.sep.join([dir, file])
            if not file.endswith("xlsx") or file.startswith("~$"):
                os.remove(filepath)
                continue
            history = History(
                filename=file,
                status="В процессе"
            )
            filedata = pd.read_excel(filepath)
            for _, value in filedata.iterrows():
                if not is_headers_correct(value):
                    continue
                date = get_correct_date(value.Rep_dt)
                delta = get_correct_delta(value.Delta)
                dt = Data(
                    rep_dt=date,
                    delta=delta,
                    history=history
                )
                dt.update()
                history.status = "Успешно"
                history.update()
                done = True
            if not done:
                history.status = "Ошибка"
                history.update()
                os.remove(filepath)
                continue
            os.remove(filepath)
        time.sleep(time_delta)


def get_correct_date(date):
    try:
        date = datetime.strptime(date, "%d.%m.%Y")
    except ValueError:
        try:
            date = datetime.strptime(date, "%Y-%m-%d")
        except ValueError:
            return
    return date


def get_correct_delta(delta):
    try:
        float(delta)
    except ValueError:
        delta = str(delta).replace(",", ".")
    try:
        return float(delta)
    except ValueError:
        return


def is_headers_correct(value):
    try:
        value.Rep_dt
        value.Delta
    except AttributeError:
        return False
    return True


if __name__ == '__main__':
    db.create_all()
    t = Thread(target=look_for_files, args=(DELTA, FILES_DIR, ))
    t.start()
    app.run(host='0.0.0.0', port=9090, debug=False)
