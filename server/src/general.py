from flask import Flask
from flask_sqlalchemy import SQLAlchemy

DELTA = 60
FILES_DIR = "server/static/uploads"

app = Flask(
    __name__,
    template_folder='../template',
    static_folder='../static'
)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['API_KEY'] = 'f93412b5-b380-4420-b65f-adc694195102'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../database.db'

db = SQLAlchemy(app)
