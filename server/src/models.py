from datetime import datetime

from src.general import db


class Updater:
    def update(self):
        db.session.add(self)
        db.session.commit()


class History(db.Model, Updater):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100))
    url = db.Column(db.String(100))
    upload_date = db.Column(db.DateTime, default=datetime.utcnow)
    status = db.Column(db.String(100))


class Data(db.Model, Updater):
    id = db.Column(db.Integer, primary_key=True)
    rep_dt = db.Column(db.DateTime)
    delta = db.Column(db.Float)
    history = db.relationship(History, backref="datas")
    history_id = db.Column(db.Integer, db.ForeignKey('history.id'))
